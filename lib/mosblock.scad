module mosblock(dimensions = [10, 10, 10], chamfer = 0, center = false) {
    
    



translate([center ? 0 : dimensions.x / 2, center ? 0 : dimensions.y / 2, center ? 0 : dimensions.z / 2]) {
    hull() {
        
        // z axis
        color("red") {
            linear_extrude(height = dimensions.z, center = true) {
                square(size =  [dimensions.x - (chamfer * 2), dimensions.y - (chamfer * 2)], center = true);
            }
        }


        // y axis
        color("green") {
            rotate([90, 0, 0]) {
                linear_extrude(height = dimensions.y, center = true) {
                    square(size =  [dimensions.x - (chamfer * 2), dimensions.z - (chamfer * 2)], center = true);
                }
            }
        }
        
      
        // x axis
        color("blue") {
            rotate([90, 0, 90]) {
                linear_extrude(height = dimensions.x, center = true) {
                    square(size =  [dimensions.y - (chamfer * 2), dimensions.z - (chamfer * 2)], center = true);
                }
            }
        }

    }
} 

}
//color("orange") mosblock([10, 20, 30]);
color("salmon") mosblock([10, 20, 30], chamfer = 2);
