use <lib/mosblock.scad>;


module test1() {
    //color("orange") mosblock([10, 20, 30]);
    color("salmon") mosblock([10, 20, 30], chamfer = 2);
}

module test2() {
    mosblock([20, 20, 20], chamfer = 5, center = true);
}


//test1();
test2();